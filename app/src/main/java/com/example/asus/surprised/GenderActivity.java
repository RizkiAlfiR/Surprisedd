package com.example.asus.surprised;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GenderActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);

        Button button_male = (Button) findViewById(R.id.button_male);
        button_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Male_GenderActivity.class);
                startActivity(intent);
            }
        });

        Button button_female = (Button) findViewById(R.id.button_female);
        button_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Female_GenderActivity.class);
                startActivity(intent);
            }
        });
    }
}
